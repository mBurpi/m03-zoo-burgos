package zoo.burgos.inspedralbes;

public class Zebra extends Animal {
	
	final static Mida mida = Mida.MITJA;
	final static Dieta dieta = Dieta.HERBIVOR;

	public Zebra(String nom, String codi, int any) {
		super(nom, codi, any);
		setMida(mida);
		setDieta(dieta);
	}

}
