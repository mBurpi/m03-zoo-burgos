package zoo.burgos.inspedralbes;

public class OsPolar extends Animal{
	final static Mida mida = Mida.GRAN;
	final static Dieta dieta = Dieta.CARNIVOR;

	public OsPolar(String nom, String codi, int any) {
		super(nom, codi, any);
		setMida(mida);
		setDieta(dieta);
		// TODO Auto-generated constructor stub
	}

}
