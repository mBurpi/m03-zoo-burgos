package zoo.burgos.inspedralbes;

public class Aguila extends Animal {

	final static Mida mida = Mida.MITJA;
	final static Dieta dieta = Dieta.CARNIVOR;

	public Aguila(String nom, String codi, int any) {
		super(nom, codi, any);
		setMida(mida);
		setDieta(dieta);
		// TODO Auto-generated constructor stub
	}
}
