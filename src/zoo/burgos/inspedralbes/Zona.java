package zoo.burgos.inspedralbes;

public class Zona {

//atributs
	private String codi;
	private int capacitat;
	Animal[] animals;

	// constructor
	public Zona(String codi, int capacitat) {
		setCodi(codi);
		setCapacitat(capacitat);
		inicialitzaAnimalsZona();
	}

	// getters and setters
	public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	public int getCapacitat() {
		return capacitat;
	}

	public void setCapacitat(int capacitat) {
		this.capacitat = capacitat;
	}
	
	public Animal[] getAnimals() {
		return animals;
	}

	// metodes

	public String toString() {
		String informacio = "\n\nZona: " + getCodi() + "\nCapacitat: " + getCapacitat()
				+ "\nAnimals en aquesta zona: ";

		for (int i = 0; i < animals.length; i++) {
			if (animals[i] != null) {
				informacio += animals[i];
			}
		}
		return informacio;
	}

	// metode per inicialitzar l'array d'animals de cada zona
	public boolean inicialitzaAnimalsZona() {
		for (int i = 0; i < this.getCapacitat(); i++) {
			animals = new Animal[this.getCapacitat()];
		}
		return true;
	}

	// metode per a afegir animals a la zona.  Recorre l'array d'animals de la zona i
	// compara amb animal[i] si poden conviure.  Si no poden, retorna false.
	// segurament hi ha una manera mes eficient que ferli 2 passades a l'array pero ara tinc la neurona requemada
	public boolean afegir(Animal animal) {
		boolean conviure = true;
		for (int i = 0; i < capacitat; i++) {
			if (animals[i] != null) {
				if (!animal.potConviure(animals[i])) {
					conviure = false;
				}
			}
		}
		for (int j = 0; j < capacitat; j++) {
			if (((animals[j] == null)) && (conviure)) {
				animals[j] = animal;
				return true;
			}
		}
		return false;
	}

}
