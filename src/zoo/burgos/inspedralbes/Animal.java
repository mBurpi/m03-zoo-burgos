package zoo.burgos.inspedralbes;

public abstract class Animal {

	protected enum Mida {
		PETIT, MITJA, GRAN;
	}

	protected enum Dieta {
		CARNIVOR, HERBIVOR;
	}

	protected String nom;
	protected String codi;
	protected int any;
	protected Mida mida;
	protected Dieta dieta;
	protected int valorMida;

	public Animal(String nom, String codi, int any) {
		setNom(nom);
		setCodi(codi);
		setAny(any);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	public int getAny() {
		return any;
	}

	public void setAny(int any) {
		this.any = any;
	}

	public Mida getMida() {
		return mida;
	}

	public void setMida(Mida mida) {
		this.mida = mida;
		if (mida == Mida.PETIT) {
			this.valorMida = 1;
		} else if (mida == Mida.MITJA) {
			this.valorMida = 2;
		} else {
			this.valorMida = 3;
		}
	}

	public int getValorMida() {
		return this.valorMida;
	}

	public Dieta getDieta() {
		return dieta;
	}

	public void setDieta(Dieta dieta) {
		this.dieta = dieta;
	}

	// metodes
	public String toString() {
		String informacio = "";
		if (this != null) {
			String className = getClass().getSimpleName();
			informacio = "\n" + className + " // " + getNom() + " / Codi: " + getCodi()
					+ " / Any naixement: " + getAny() + " / Mida: " + getMida() + " / Dieta: "
					+ getDieta();
		}
		return informacio;
	}

	// metode per saber si un animal pot conviure amb altre  (Veure diagrames de flux)
	public boolean potConviure(Animal animal) {

		// l'animal que entra es HERBIVOR
		if (getDieta().equals(Dieta.HERBIVOR)) {
			// l'animal que es compara es HERBIVOR
			if (getDieta().equals(animal.getDieta())) {
				return true;
			}
			// l'animal que es compara es CARNIVOR
			else {
				// l'animal que entra es MES GRAN que el que es compara
				if (valorMida > animal.valorMida) {
					return true;
				}
				// l'animal que entra NO ES MES GRAN
				else {
					return false;
				}
			}
		}
		// l'animal que entra es CARNIVOR
		else {
			// l'animal que es compara tambe es CARNIVOR
			if (getDieta() == animal.getDieta()) {
				// els dos animals son CARNIVORS i de la mateixa ESPECIE
				if (getClass().getSimpleName().equals(animal.getClass().getSimpleName())) {
					return true;
				} // no son de la mateixa ESPECIE
				else {
					return false;
				}
			}
			// animal que entra=CARNIVOR, animal que compara=HERBIVOR
			else {
				if (getValorMida() < animal.getValorMida()) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
	

}