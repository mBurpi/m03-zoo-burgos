package zoo.burgos.inspedralbes;

public class PrincipalProves {

	public static void main(String[] args) {

		// test
		// Es construeix un objecte Zoo
		Zoo bcn = new Zoo("Zoo de Barcelona", 4);
		bcn.crearZones(bcn.getNZones());
		Zoo mad = new Zoo("Parque Zoologico de Madrid", 2);
		mad.crearZones(mad.getNZones());

		// S'inicien les zones amb el seu nom, capacitat, i animals que hi ha a dins.
//		bcn.zones[0] = new Zona("b01", 4);
//		bcn.zones[1] = new Zona("b02", 4);
//		bcn.zones[2] = new Zona("b03", 3);
//		bcn.zones[3] = new Zona("b04", 5);
		

		// es creen parelles d'animals PETITS, MITJANS i GRANS / CARNIVORS I HERBIVORS
		Animal koala1 = new Koala("Pacoala", "005H", 1995);
		Animal koala2 = new Koala("Paquita", "006H", 1999);
		Animal zebra1 = new Zebra("Andreu", "001H", 2010);
		Animal zebra2 = new Zebra("Blai", "002H", 2015);
		Animal giraf1 = new Girafa("Carles", "0003H", 1990);
		Animal giraf2 = new Girafa("Dominica", "0004H", 1996);
		Animal aguila2 = new Aguila("Elena", "003C", 1996);
		Animal aguila1 = new Aguila("Gerard", "001C", 2019);
		Animal lleo1 = new Lleo("Hector", "002C", 1992);
		Animal lleo2 = new Lleo("Felipe", "004C", 1998);
		Animal os1 = new OsPolar("Wendy", "005C", 1992);
		Animal os2 = new OsPolar("Ozzy", "006C", 2002);

	
//		//1 cas
//		mad.zones[0] = new Zona("m01", 3);
//		mad.zones[1] = new Zona("m02", 2);
//		mad.afegir(lleo1);
//		mad.afegir(lleo2);
//		mad.afegir(zebra1);
//		mad.afegir(zebra2);
//		//print a pel
//		System.out.println(mad.toString());

		//2 cas
//		mad.zones[0] = new Zona("m01", 2);
//		mad.zones[1] = new Zona("m02", 2);
//		mad.afegir(lleo1);
//		mad.afegir(zebra1);
//		mad.afegir(lleo2);
//		mad.afegir(zebra2);
//		//print a pel
//		System.out.println(mad.toString());
//		mad.traslladar();
//		//print tots a la reserva
//		System.out.println(mad.toString());
//		mad.recolocar();
//		//print recolocats
//		System.out.println(mad.toString());
		
//		//3 cas
//		mad.zones[0] = new Zona("m01", 3);
//		mad.zones[1] = new Zona("m02", 2);
//		mad.afegir(zebra1);
//		mad.afegir(lleo1);
//		mad.afegir(zebra2);
//		mad.afegir(lleo2);
//		mad.afegir(zebra1);
//		//print a pel
//		System.out.println(mad.toString());
//		mad.traslladar();
//		//print tots a la reserva
//		System.out.println(mad.toString());
//		mad.recolocar();
//		//print recolocats
//		System.out.println(mad.toString());
		
		//4 cas
		mad.zones[0] = new Zona("m01", 2);
		mad.zones[1] = new Zona("m02", 3);
		mad.afegir(zebra1);
		mad.afegir(lleo1);
		mad.afegir(zebra2);
		mad.afegir(lleo2);
		mad.afegir(zebra1);
		//print a pel
		System.out.println(mad.toString());
		mad.traslladar();
		//print tots a la reserva
		System.out.println(mad.toString());
		mad.recolocar();
		//print recolocats
		System.out.println(mad.toString());
		
		
		
		
	// ...	
		
		
		

//		mad.afegir(aguila1);
//		mad.afegir(giraf1);
//		mad.afegir(koala1);
//		mad.afegir(aguila2);
//		mad.afegir(zebra2);
////		mad.afegir(os1);
//		mad.afegir(giraf2);
//		mad.afegir(koala2);
//		mad.afegir(lleo2);
//		mad.afegir(os2);
		
		// es fiquen els animals a la zona
//		bcn.afegir(zebra1);
//		bcn.afegir(lleo1);
//		bcn.afegir(aguila1);
//		bcn.afegir(giraf1);
//		bcn.afegir(koala1);
//		bcn.afegir(aguila2);
//		bcn.afegir(zebra2);
//		bcn.afegir(os1);
//		bcn.afegir(giraf2);
//		bcn.afegir(koala2);
//		bcn.afegir(lleo2);
//		bcn.afegir(os2);

//		System.out.println(bcn.toString());
//		bcn.traslladar();
//		System.out.println(bcn.toString());
//		bcn.recolocar();
//		System.out.println(bcn.toString());

//		System.out.println(mad.toString());
//		mad.traslladar();
//		System.out.println(mad.toString());
//		mad.recolocar();
//		System.out.println(mad.toString());

	}

}
