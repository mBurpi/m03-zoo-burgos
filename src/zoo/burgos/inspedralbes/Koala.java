package zoo.burgos.inspedralbes;

public class Koala extends Animal {
	final static Mida mida = Mida.PETIT;
	final static Dieta dieta = Dieta.HERBIVOR;

	public Koala(String nom, String codi, int any) {
		super(nom, codi, any);
		setMida(mida);
		setDieta(dieta);
		// TODO Auto-generated constructor stub
	}
}
