package zoo.burgos.inspedralbes;

import zoo.burgos.inspedralbes.Animal.Dieta;

public class Zoo {

	private String nom;
	private int nZones;
	Zona[] zones;
	static Animal[] reserva = new Animal[99];
	static int n = 0;

	// constructor
	public Zoo(String nom, int nZones) {
		setNom(nom);
		setNZones(nZones);
	}

	// getters i setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNZones() {
		return nZones;
	}

	public void setNZones(int nZones) {
		this.nZones = nZones;
	}

	// metodes
	public String toString() {
		String informacio = "\nNom del Zoo: " + this.getNom() + "\nNumero de zones: " + this.getNZones();
		for (int i = 0; i < zones.length; i++) {
			informacio += zones[i].toString();
		}
		informacio += "\n\nReserva:";
		for (int i = 0; i < reserva.length; i++) {
			if (reserva[i] != null) {
				informacio += reserva[i];
			}
		}
		return informacio;
	}

	// metode per comptar el num d'animals que hi ha en l'array,
	// ja que iniciem array.length amb dimensio maxima
	public int contaAnimals(Animal[] a) {
		int nAnimals = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] != null) {
				nAnimals++;
			}
		}
		return nAnimals;
	}

	// metode per a crear les zones en funcio del nZones indicat en el constructor
	// de Zoo i que inicialitza un array d'animals a cada zona
	public void crearZones(int nZones) {
		zones = new Zona[nZones];
	}

	// metode que recorre l'array de zones i crida al metode afegir() de la classe
	// Zona
	// Pot passar que la zona estigui plena o les regles de convivencia no permetin
	// ficar l'animal. Llavors es guarda en una reserva del zoo per ser tractat
	// posteriorment amb el metode recolocar()
	public boolean afegir(Animal animal) {
		for (int i = 0; i < zones.length; i++) {
			if (zones[i].afegir(animal)) {
				 System.out.println("S'ha afegit " + animal.getClass().getSimpleName() + " / "
				 + animal.getNom() + " a la zona " + zones[i].getCodi());
				return true;
			}
		}
		// si l'animal no hi cap a cap zona, va a una reserva static
		reserva[n] = animal;
		n++;
		// System.out.println("[*] L'animal " + animal.getClass().getSimpleName() + " /
		// " + animal.getNom()
		// + " s'ha enviat a la reserva");
		return false;
	}

	// metode que treu tots els animals de les zones i els fica en la reserva.
	// abans de fer la recolocacio dels animals netejem les zones portant els
	// animals a la reserva, es classifiquen per Dieta amb el metode ordenar()
	public void traslladar() {
		for (int i = 0; i < zones.length; i++) {
			for (int j = 0; j < zones[i].getAnimals().length; j++) {
				if (zones[i].animals[j] != null) {
					reserva[n] = zones[i].animals[j];
					zones[i].animals[j] = null;
					n++;
				}
			}
		}
		ordenar(reserva);
	}

	// metode que ordena la reserva, els CARNIVORS primers de llista. esta pensat
	// per
	// que rebi l'array de la reserva despr�s de buidar les zones
	public void ordenar(Animal[] a) {
		int nAnimals = contaAnimals(reserva);
		Animal[] aux = new Animal[nAnimals];
		int j = 0;
		int k = 1;
		for (int i = 0; i < nAnimals; i++) {
			if (a[i].dieta.equals(Dieta.CARNIVOR)) { // o be es podria crear classes carnivor/herbivor i fer instanceof
				aux[j] = a[i];
				j++;
			} else {
				aux[nAnimals - k] = a[i];
				k++;
			}
		}
		reserva = aux;
	}

	// metode que recoloca els animals desde la reserva a les zones.
	// Comenca colocant els carnivors i implementa el metode afegir de la mateixa
	// manera, si hi ha animals que no hi caben, es copien en un array auxiliar que
	// despr�s es copia a la reserva vertadera.
	public void recolocar() {
		Animal[] reReserva = new Animal[reserva.length];
		int j = 0;
		for (int i = 0; i < reserva.length; i++) {
			if (reserva[i] != null) {
				if (afegir(reserva[i])) {
					reserva[i] = null;
				} else {
					reReserva[j] = reserva[i];
					j++;
				}
			}
		}
		n=0;
		int nAnimals = contaAnimals(reReserva);
		for (int i = 0; i < nAnimals; i++) {
			reserva[i] = reReserva[i];
			n++;
		}

	}

}
