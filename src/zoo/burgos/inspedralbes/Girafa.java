package zoo.burgos.inspedralbes;

public class Girafa extends Animal{
	
	final static Mida mida = Mida.GRAN;
	final static Dieta dieta = Dieta.HERBIVOR;

	public Girafa(String nom, String codi, int any) {
		super(nom, codi, any);
		setMida(mida);
		setDieta(dieta);
		// TODO Auto-generated constructor stub
	}

}
